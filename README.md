<div style="text-align: center;">
<img src="./img/intro.png" style="border-radius: 25px;">
</div>

<h1>توضیحات پروژه</h1>
<p>سلام، این پروژه یک فروشگاه آنلاین به زبان پایتون و فریم ورک جنگو هست که شامل 8 بخش به همراه CRUD روی کل بخش های پروژه هست
</p>
<p style="text-align:left" dir="ltr">
- User 
<br>
- Maneger
<br>
- Admin
<br>
- Order
<br>
- OrderDetails
<br>
- Cart
<br>
- Product
<br>
- Category
</p>

<h1>اجرای پروژه</h1>
<p>
اول از همه جنگو رو روی سیستمتون با این دستور نصب کنید
</p>
<div style="text-align:left" dir="ltr">
<code > pip install django</code>
</div>
<p>
بعد با این دستور پروژه رو کلون کنید
</p>
<div style="text-align:left" dir="ltr">
<code > git clone https://gitlab.com/azadit441/shopbackend.git</code>
</div>
<p>
و وارد فولدر پروژه بشید برای اجرای پروژه از این دستور استفاده کنید : 
</p>
<div style="text-align:left" dir="ltr">
<code > python manage.py runserver</code>
</div>

<h1>زبان و ابزار های استفاده شده</h1>
<p>توی این پروژه ما از زبان پایتون و فریم ورک جنگو استفاده کردیم و دیتا ها در دیتابیس SQLite ذخیره میشن (:</p>
<br>
<p>در فایل postman پروژه تمام API ها و متد ها پیاده و تعریف شده اند</p>
<img src="./img/sqlite.png" width="60">
<img src="./img/py.png" width="60">
<img src="./img/django.svg" width="40">
<img src="./img/postman.svg" width="50">
