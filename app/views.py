from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
import json


# Tables
from .models import Users
from .models import Products
from .models import Categorys
from .models import Orders
from .models import OrderDetails
from .models import Carts
from .models import Maneger
from .models import Admin


# User
@api_view(['GET'])
def dataUsers(request):
    usersList = Users.objects.all()

    data = []
    for user in usersList:
        userData = {
            'nameLastname': user.name_lastname,
            'username': user.username,
            'password': user.password,
            'email': user.email,
            'phoneNumber': user.phone_number,
            'city': user.city,
            'address': user.address,
            'nationalCode': user.national_code
        }
        data.append(userData)

    return JsonResponse({'data': data})


@api_view(['POST'])
def addUser(request):
    nameLastname = request.POST.get('nameLastname')
    username = request.POST.get('username')
    password = request.POST.get('password')
    email = request.POST.get('email')
    phoneNumber = request.POST.get('phoneNumber')
    city = request.POST.get('city')
    address = request.POST.get('address')
    nationalCode = request.POST.get('nationalCode')

    user = Users(
    username = nameLastname,
    name_lastname = username,
    password = password,
    email = email,
    phone_number = phoneNumber,
    city = city,
    address = address,
    national_code = nationalCode,
    )
    user.save()

    return Response({'status': 'ok'})

@api_view(['DELETE'])
def deleteUser(request):
    userId = request.POST.get('id')

    Users.objects.filter(id=userId).delete()

    return JsonResponse({'ok': 'true', 'code' : '200'})



@api_view(['PUT'])
def updateUser(request):
    userId = request.POST.get('id')
    nameLastname = request.POST.get('nameLastname')
    username = request.POST.get('username')
    password = request.POST.get('password')
    email = request.POST.get('email')
    phoneNumber = request.POST.get('phoneNumber')
    city = request.POST.get('city')
    address = request.POST.get('address')
    nationalCode = request.POST.get('nationalCode')

    user = Users.objects.get(id=userId)
    user.username = username
    user.name_lastname = nameLastname
    user.password = password
    user.email = email
    user.phone_number = phoneNumber
    user.city = city
    user.address = address
    user.national_code = nationalCode
    user.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})


#Manager
@api_view(['GET'])
def dataManeger(request):
    manegerList = Maneger.objects.all()

    data = []
    for maneger in manegerList:
        manegerData = {
            'name': maneger.name,
            'username': maneger.username,
            'password' :maneger.password
        }
        data.append(manegerData)

    return JsonResponse({'data': data})


@api_view(['PUT'])
def updateManeger(request):
    manegerId = request.POST.get('id')
    name = request.POST.get('name')
    username = request.POST.get('username')
    password = request.POST.get('password')

    maneger = Maneger.objects.get(id=manegerId)
    maneger.name = name
    maneger.username = username
    maneger.password = password

    maneger.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})


# Product
@api_view(['GET'])
def dataProduct(request):
    productList = Products.objects.all()

    data = []
    for product in productList:
        productData = {
            'productName': product.product_name,
            'description': product.description,
            'price' :product.price,
            'image' : product.image,
            'categoryId' : product.category_id.to_dict()
        }
        data.append(productData)

    return JsonResponse({'data': data})


@api_view(['POST'])
def addProduct(request):
    productName = request.POST.get('productName')
    description = request.POST.get('description')
    price = request.POST.get('price')
    image = request.POST.get('image')
    categoryId = request.POST.get('categoryId')

    category = Categorys.objects.get(id=categoryId)

    product = Products(
    product_name = productName,
    description = description,
    price = price,
    image = image,
    category_id = category
    )
    product.save()

    return Response({'status': 'ok'})


@api_view(['DELETE'])
def deleteProduct(request):
    productId = request.POST.get('id')

    Products.objects.filter(id=productId).delete()

    return JsonResponse({'ok': 'true', 'code' : '200'})



@api_view(['PUT'])
def updateProduct(request):
    productId = request.POST.get('id')
    productName = request.POST.get('productName')
    description = request.POST.get('description')
    price = request.POST.get('price')
    image = request.POST.get('image')
    categoryId = request.POST.get('categoryId')

    product = Products.objects.get(id=productId)
    product.product_name = productName
    product.description = description
    product.price = price
    product.image = image
    product.category_id = Categorys.objects.get(id=categoryId)

    product.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})




# Category
@api_view(['GET'])
def dataCategory(request):
    categoryList = Categorys.objects.all()
    data = []
    for category in categoryList:
        categoryData = {
            'category_name': category.category_name,
            'category_description': category.category_description,
            'category_image' :category.category_image
        }
        data.append(categoryData)

    return JsonResponse({'data': data})


@api_view(['POST'])
def addCategory(request):
    categoryName = request.POST.get('categoryName')
    categoryDescription = request.POST.get('categoryDescription')
    categoryImage = request.POST.get('categoryImage')


    category = Categorys(
    category_name = categoryName,
    category_description = categoryDescription,
    category_image = categoryImage
    )
    category.save()

    return Response({'status': 'ok'})


@api_view(['DELETE'])
def deleteCategory(request):
    categoryId = request.POST.get('id')

    Categorys.objects.filter(id=categoryId).delete()

    return JsonResponse({'ok': 'true', 'code' : '200'})



@api_view(['PUT'])
def updateCategory(request):
    categoryId = request.POST.get('id')
    categoryName = request.POST.get('categoryName')
    categoryDescription = request.POST.get('categoryDescription')
    categoryImage = request.POST.get('categoryImage')

    category = Categorys.objects.get(id=categoryId)
    category.category_name = categoryName
    category.category_description = categoryDescription
    category.category_image = categoryImage

    category.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})




# Cart
@api_view(['GET'])
def dataCart(request):
    cartList = Carts.objects.all()

    data = []
    for cart in cartList:
        cartData = {
            'userId': cart.user_id_id,
            'productId': cart.product_id_id,
            'quantity': cart.quantity,
            'status': cart.status
        }
        data.append(cartData)

    return JsonResponse({'data': data})


@api_view(['POST'])
def addCart(request):
    userId = request.POST.get('userId')
    productId = request.POST.get('productId')
    quantity = request.POST.get('quantity')
    status = request.POST.get('status')

    user = Users.objects.get(id=userId)
    product = Products.objects.get(id=productId)

    cart = Carts(
    user_id = user,
    product_id = product,
    quantity = quantity,
    status = status
    )
    cart.save()
    return Response({'status': 'ok'})

@api_view(['DELETE'])
def deleteCart(request):
    cartId = request.POST.get('id')


    Carts.objects.filter(id=cartId).delete()

    return JsonResponse({'ok': 'true', 'code' : '200'})



@api_view(['PUT'])
def updateCart(request):
    cartId = request.POST.get('id')
    userId = request.POST.get('userId')
    productId = request.POST.get('productId')
    quantity = request.POST.get('quantity')
    status = request.POST.get('status')

    cart = Carts.objects.get(id=cartId)
    cart.user_id = Users.objects.get(id=userId)
    cart.product_id = Products.objects.get(id=productId)
    cart.quantity = quantity
    cart.status = status

    cart.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})




# Order
@api_view(['GET'])
def dataOrder(request):
    orderList = Orders.objects.all()

    data = []
    for order in orderList:
        orderData = {
            'userId': order.user_id_id,
            'orderData': order.order_data,
            'totalAmount': order.total_amount,
            'paymentType': order.payment_type,
            'status': order.status

        }
        data.append(orderData)

    return JsonResponse({'data': data})



@api_view(['POST'])
def addOrder(request):
    userId = request.POST.get('userId')
    orderData = request.POST.get('orderData')
    totalAmount = request.POST.get('totalAmount')
    paymentType = request.POST.get('paymentType')
    status = request.POST.get('status')

    order = Orders(
    user_id = Users.objects.get(id=userId),
    order_data = orderData,
    total_amount = totalAmount,
    payment_type = paymentType,
    status = status,
    )
    order.save()

    return Response({'status': 'ok'})

@api_view(['DELETE'])
def deleteOrder(request):
    orderId = request.POST.get('id')


    Orders.objects.filter(id=orderId).delete()

    return JsonResponse({'ok': 'true', 'code' : '200'})



@api_view(['PUT'])
def updateOrder(request):
    orderId = request.POST.get('id')
    userId = request.POST.get('userId')
    orderData = request.POST.get('orderData')
    totalAmount = request.POST.get('totalAmount')
    paymentType = request.POST.get('paymentType')
    status = request.POST.get('status')

    order = Orders.objects.get(id=orderId)
    order.user_id = Users.objects.get(id=userId)
    order.order_data = orderData
    order.total_amount = totalAmount
    order.payment_type = paymentType
    order.status = status

    order.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})




# orderDetails
@api_view(['GET'])
def dataOrderDetails(request):
    OrderDetailsList = OrderDetails.objects.all()

    data = []
    for orderDetail in OrderDetailsList:
        orderDetailsData = {
            'oredrId': orderDetail.oredr_id_id,
            'productId': orderDetail.product_id_id,
            'quantity': orderDetail.quantity,
            'itemNotes': orderDetail.item_notes,
            'itemPrice': orderDetail.item_price,
            'itemDiscount': orderDetail.item_discount,
            'itemTotal': orderDetail.item_total,
            'itemStatus': orderDetail.item_status
        }
        data.append(orderDetailsData)

    return JsonResponse({'data': data})


@api_view(['POST'])
def addOrderDetails(request):
    oredrId = request.POST.get('oredrId')
    productId = request.POST.get('productId')
    quantity = request.POST.get('quantity')
    itemNotes = request.POST.get('itemNotes')
    itemPrice = request.POST.get('itemPrice')
    itemDiscount = request.POST.get('itemDiscount')
    itemTotal = request.POST.get('itemTotal')
    itemStatus = request.POST.get('itemStatus')

    orderdetail = OrderDetails(
    oredr_id = Orders.objects.get(id=oredrId),
    product_id = Products.objects.get(id=oredrId),
    quantity = quantity,
    item_notes = itemNotes,
    item_price = itemPrice,
    item_discount = itemDiscount,
    item_total = itemTotal,
    item_status = itemStatus
    )
    orderdetail.save()

    return Response({'status': 'ok'})

@api_view(['DELETE'])
def deleteOrderDetails(request):
    orderDetailId = request.POST.get('id')


    OrderDetails.objects.filter(id=orderDetailId).delete()

    return JsonResponse({'ok': 'true', 'code' : '200'})



@api_view(['PUT'])
def updateOrderDetails(request):
    orderDetailId = request.POST.get('id')
    oredrId = request.POST.get('oredrId')
    productId = request.POST.get('productId')
    quantity = request.POST.get('quantity')
    itemNotes = request.POST.get('itemNotes')
    itemPrice = request.POST.get('itemPrice')
    itemDiscount = request.POST.get('itemDiscount')
    itemTotal = request.POST.get('itemTotal')
    itemStatus = request.POST.get('itemStatus')

    orderdetail = OrderDetails.objects.get(id=orderDetailId)
    orderdetail.oredr_id = Orders.objects.get(id=oredrId) 
    orderdetail.product_id = Products.objects.get(id=productId)  
    orderdetail.quantity = quantity
    orderdetail.item_notes = itemNotes
    orderdetail.item_price = itemPrice
    orderdetail.item_discount = itemDiscount
    orderdetail.item_total = itemTotal
    orderdetail.item_status = itemStatus

    orderdetail.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})



# Admin
@api_view(['GET'])
def dataAdmin(request):
    adminsList = Admin.objects.all()

    data = []
    for admin in adminsList:
        adminData = {
            'name': admin.name,
            'username': admin.username,
            'password': admin.password
        }
        data.append(adminData)

    return JsonResponse({'data': data})


@api_view(['POST'])
def addAdmin(request):
    name = request.POST.get('name')
    username = request.POST.get('username')
    password = request.POST.get('password')

    admin = Admin(
    name = name,
    username = username,
    password = password
    )
    admin.save()

    return Response({'status': 'ok'})

@api_view(['DELETE'])
def deleteAdmin(request):
    adminId = request.POST.get('id')


    Admin.objects.filter(id=adminId).delete()

    return JsonResponse({'ok': 'true', 'code' : '200'})


@api_view(['PUT'])
def updateAdmin(request):
    adminId = request.POST.get('id')
    name = request.POST.get('name')
    username = request.POST.get('username')
    password = request.POST.get('password')

    admin = Admin.objects.get(id=adminId)
    name = name,
    username = username,
    password = password

    admin.save()

    return JsonResponse({'ok': 'true', 'code' : '200'})
