from django.urls import path
from . import views

urlpatterns = [
    # CRUD USERS
    path('add/user', views.addUser , name='add_users'),
    path('data/users', views.dataUsers , name='data_users'),
    path('delete/user', views.deleteUser , name='delete_users'),
    path('update/user', views.updateUser , name='update_users'),
    

    # CRUD Maneger
    path('data/maneger', views.dataManeger , name='data_maneger'),
    path('update/maneger', views.updateManeger , name='update_maneger'),

    # CRUD Product
    path('add/product', views.addProduct , name='add_product'),
    path('data/products', views.dataProduct , name='data_products'),
    path('delete/product', views.deleteProduct , name='delete_products'),
    path('update/product', views.updateProduct , name='update_products'),

    # CRUD Category
    path('add/category', views.addCategory , name='add_category'),
    path('data/categorys', views.dataCategory , name='data_categories'),
    path('delete/category', views.deleteCategory , name='delete_categories'),
    path('update/category', views.updateCategory , name='update_categories'),


    # CRUD Cart
    path('add/cart', views.addCart , name='add_cart'),
    path('data/carts', views.dataCart , name='data_carts'),
    path('delete/cart', views.deleteCart , name='delete_cart'),
    path('update/cart', views.updateCart , name='update_cart'),


    # CRUD Order
    path('add/order', views.addOrder , name='add_order'),
    path('data/orders', views.dataOrder , name='data_orders'),
    path('delete/order', views.deleteOrder , name='delete_order'),
    path('update/order', views.updateOrder , name='update_order'),

    # CRUD OrderDetails
    path('add/orderDetails', views.addOrderDetails , name='add_orderDetails'),
    path('data/orderDetails', views.dataOrderDetails , name='data_orderDetails'),
    path('delete/orderDetails', views.deleteOrderDetails , name='delete_orderDetails'),
    path('update/orderDetails', views.updateOrderDetails , name='update_orderDetails'),


    # CRUD Admin
    path('add/admin', views.addAdmin , name='add_admin'),
    path('data/admins', views.dataAdmin , name='data_admin'),
    path('delete/admin', views.deleteAdmin , name='delete_admin'),
    path('update/admin', views.updateAdmin , name='update_admin')
]